const http = require("http");
const url = require("url");
const fs = require("fs");
const mime = require("mime-types");
const mysql = require("mysql");
const passwordHash = require("password-hash");

const PORT = 5500;

var con = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "dtb456",
    database: "database"
});

con.connect(function(err) {
    if (err) throw err;
    console.log("Connected!");
});

/**
 * --apiRequestHandlers--
 * Tt is container for all api request handlers
 * divided by request type (GET, POST,...).
 * -HANDLER-
 * parameters:
 * first param. (args) - array of values in link
 * second param. (body) - body of request as object
 */

var apiRequestHandlers = {
    GET: {
        ////////////////////////OLD/////////////////////////////////
        /*"test": function(args, body) {
            return {
                type: "GET",
                status: 200,
                description: "this is test request",
                args: args
            };
        },*/
        ////////////////////////////////////////////////////
        "posts": function (args, body) {
            return new Promise(function (resolve, reject) {
                con.query('SELECT * FROM posts', function (err, result) {
                    if (err) reject(err);
                    resolve(result);
                });
            });
        },
        "images": function (args, body) {
            return new Promise(function (resolve, reject) {
                con.query('SELECT * FROM image', function (err, result) {
                    if (err) reject(err);
                    resolve(result);
                });
            });
        },
        "image" : function (args, body) {
            return new Promise(function (resolve, reject) {
                con.query(`SELECT img FROM image WHERE id=${args[0]}`, function (err, result) {
                    if (err) reject(err);
                    resolve(result);
                });
            });
        },
        "facts" : function (args, body) {
            return new Promise(function (resolve, reject) {
                con.query('SELECT * FROM zaujimavosti', function (err, result) {
                    if (err) reject(err);
                    resolve(result);
                });
            });
        },
        "deletedFacts" : function (args, body) {
            return new Promise(function (resolve, reject) {
                con.query('SELECT * FROM vymazanezaujimavosti', function (err, result) {
                    if (err) reject(err);
                    resolve(result);
                });
            });
        }
    },
    POST: {
        "addPost": function (args, body) {
            return new Promise(function (resolve, reject) {
                con.query(`INSERT INTO posts(author, content, title) VALUES('${body.author}', '${body.content}', '${body.title}')`, function (err, result) {
                    if (err) reject(err);
                });
                con.query('SELECT * FROM posts WHERE id=(SELECT MAX(id) FROM posts)', function (err, result) {
                    if (err) reject(err);
                    resolve(result);
                });
            });
        },
        "login": function (args, body) {
            return new Promise(function (resolve, reject) {
                con.query('SELECT * FROM users WHERE id=1', function (err, result) {
                    if (err) reject(err);
                    if (passwordHash.verify(body.password, result[0].password) && result[0].name == body.name) {
                        resolve({message: "Úspešne prihlásený"});
                    } else {
                        resolve({message: "Nesprávne heslo"});
                    }
                });
            })
        }
    },
    PUT: {
        "editPost": function (args, body) {
            return new Promise(function (resolve, reject) {
                con.query(`UPDATE posts SET content='${body.content}', title='${body.title}' WHERE id=${body.id}`, function (err, result) {
                    if (err) reject(err);
                    resolve(result);
                });
            });
        }
    },
    DELETE: {
        "deletePost": function (args, body) {
            return new Promise(function (resolve, reject) {
                con.query(`DELETE FROM posts WHERE id=${args[0]}`, function (err, result) {
                    if (err) reject(err);
                    resolve(result);
                });
            });
        },
        "deleteFact" : function (args, body) {
            return new Promise(function (resolve, reject) {
                con.query(`INSERT INTO vymazanezaujimavosti VALUES (${args[0]})`, function (err, result) {
                    if (err) reject(err);
                    resolve(result);
                });
            });
        },
        "revertChanges" : function (args, body) {
            return new Promise(function (resolve, reject) {
                con.query('DELETE FROM vymazanezaujimavosti', function (err, result) {
                    if (err) reject(err);
                    resolve(result);
                });
            });
        }
    }
};

http.createServer(function (req, res) {
    let q = url.parse(req.url, true);

    // API
    if (q.pathname.startsWith("/api/")) {
        let args = q.pathname.substring(5, q.pathname.length).split("/");
        let requestName = args[0];
        args.splice(0, 1);
        let requestHandler = apiRequestHandlers[req.method][requestName];
        if (requestHandler) {
            let body = "";
            req.on("data", function(data) {
                body += data;
            });
            req.on("end", function() {
                if (body === "") {
                    body = null;
                } else {
                    body = JSON.parse(body);
                }
                requestHandler(args, body).then(function (response) {
                    let responseBody = response;
                    if (responseBody && responseBody.error) {
                        res.writeHead(responseBody.code, { "Content-Type": "application/json" });
                    } else {
                        res.writeHead(200, { "Content-Type": "application/json" });
                    }
                    if (typeof(responseBody) !== "string") {
                        responseBody = JSON.stringify(responseBody);
                    }
                    return res.end(responseBody);
                });
            });
        } else {
            res.writeHead(404, { "Content-Type": "application/json" });
            return res.end(JSON.stringify({ error: "Request Not Found", code: 404 }));
        }

        // PAGES
    } else {
        if (q.pathname == "/") {
            q.pathname += "index.html";
        }
        let filename = "../Betting-angular/dist/Betting-angular" + q.pathname;
        fs.readFile(filename, function(err, data) {
            if (err) {
                res.writeHead(404, {'Content-Type': 'text/html'});
                return res.end("404 Not Found");
            }
            res.writeHead(200, mime.contentType(filename));
            res.write(data);
            return res.end();
        });
    }
}).listen(PORT);

function convertXML(xmlData) {
    let options = {
        attributeNamePrefix : "_",
        attrNodeName: "attr", //default is 'false'
        textNodeName : "#text",
        ignoreAttributes : false,
        ignoreNameSpace : false,
        allowBooleanAttributes : true,
        parseNodeValue : true,
        parseAttributeValue : false,
        trimValues: true,
        cdataTagName: "__cdata", //default is 'false'
        cdataPositionChar: "\\c",
        parseTrueNumberOnly: false,
        arrayMode: true, //"strict"
        attrValueProcessor: (val, attrName) => he.decode(val, {isAttributeValue: true}),//default is a=>a
        tagValueProcessor : (val, tagName) => he.decode(val), //default is a=>a
        stopNodes: ["parse-me-as-string"]
    };

    if (parser.validate(xmlData) === true) { //optional (it'll return an object in case it's not valid)
        return parser.parse(xmlData, options);
    }

    // Intermediate obj
    // var tObj = parser.getTraversalObj(xmlData,options);
    // var jsonObj = parser.convertToJson(tObj,options);
}

console.log(`Server is listening on port ${PORT}.`);
